<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return $app->version();
});

$app->post('/register', 'AuthController@register');
$app->post('/login', 'AuthController@login');
$app->get('/logout', 'AuthController@logout');

//BoardController
  //$app->get('/boards', ['middleware'=>'auth','BoardController@index']);
  $app->get('/boards', 'BoardController@index');
$app->post('/boards', 'BoardController@store');
$app->get('/boards/{id}', 'BoardController@show');
$app->put('/boards/{id}', 'BoardController@update');
$app->delete('/boards/{id}', 'BoardController@destroy');
//ListController
$app->group(['prefix' => '/boards/{board_id}'], function() use ($app) {
    $app->get('/lists', 'ListController@index');
    $app->post('/lists', 'ListController@store');
    $app->get('/lists/{id}', 'ListController@show');
    $app->put('/lists/{id}', 'ListController@update');
    $app->delete('/lists/{id}', 'ListController@destroy');
});
//CardController
$app->group(['prefix' => '/boards/{board_id}/lists/{list_id}'], function() use ($app) {
    $app->get('/card', 'CardController@index');
    $app->post('/card', 'CardController@store');
    $app->get('/card/{id}', 'CardController@show');
    $app->put('/card/{id}', 'CardController@update');
    $app->delete('/card/{id}', 'CardController@destroy');
});
