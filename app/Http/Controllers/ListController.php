<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Board;
use App\User;
use App\Lists;

class ListController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth',['except'=>['index']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($board_id)
    {
        $board = Board::find($board_id);
        // print "<pre>";
        // print_r(\Auth::user()->id);exit;
        if( \Auth::user()->id !== $board->user_id) {
            return response()->json(['status'=>'error', 'message'=>'unauthorized'], 401);
        }
        return response()->json(['status'=>'success', 'lists'=>$board->lists], 401);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $board_id)
    {
        $this->validate($request, ['name'=>'required']);

        $board = Board::find($board_id);

        if( \Auth::user()->id !== $board->user_id) {
            return response()->json(['status'=>'error', 'message'=>'unauthorized'], 401);
        }

        $list = $board->lists()->create([
            'name' => $request->name
        ]);
        return response()->json(['status'=>'success', 'lists'=>$list], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($board_id, $id)
    {
        $list = Board::find($board_id);

        return response()->json(['status'=>'success', 'lists'=>$list->lists()->find($id)], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $board_id, $id)
    {
      $this->validate($request, ['name'=>'required']);

      $board = Board::find($board_id);

      if( \Auth::user()->id !== $board->user_id) {
          return response()->json(['status'=>'error', 'message'=>'unauthorized'], 401);
      }

      $list = $board->lists()->find($id);
      // print "<pre>";
      // print_r($list);exit;

      $list->update([
          'name' => $request->name
      ]);
      return response()->json(['status'=>'success', 'message'=>'updated', 'lists'=>$list], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($board_id, $id)
    {
      $board = Board::find($board_id);

      if( \Auth::user()->id !== $board->user_id) {
          return response()->json(['status'=>'error', 'message'=>'unauthorized'], 401);
      }

      $list = $board->lists()->find($id);
      // print "<pre>";
      // print_r($list);exit;

      if($list->delete()) {
          return Response()->json(['status'=> 'success', 'message'=> 'Deleted'], 200);
      }

      return Response()->json(['status'=> 'error', 'message'=> 'Something wentt wrong'], 200);
    }
}
