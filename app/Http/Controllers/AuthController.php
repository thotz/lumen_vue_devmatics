<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;


class AuthController extends Controller
{

    public function __constructor()
    {
        
    }

    public function register(Request $request)
    {
        $user = User::create([
          'username' => $request->username,
          'email' => $request->email,
          'password' => app('hash')->make($request->password),
          'api_token' => str_random(50)

        ]);

        return response()->json(['user'=>$user], 200);
    }

    public function login(Request $request)
    {
        $user = User::where('email', $request->email)->first();

        if( !$user) {
            return response()->json(['status'=>'error', 'message'=> 'User not found'], 401);
        }

        if( !Hash::check($request->password, $user->password)) {
            return response()->json(['status'=>'error', 'message'=> 'invalid credential'], 401);
        }

        $user->update(['api_token' => str_random(50)]);

        return response()->json(['status'=>'success','user'=>$user], 200);
    }

    public function logout(Request $request)
    {
        $user = User::where('api_token', $request->api_token)->first();

        if( !$user) {
            return response()->json(['status'=>'error', 'message'=> 'not logged in'], 401);
        }

        $user->api_token = null;

        $user->save();

        return response()->json(['status'=>'success', 'message'=> 'You are now logged out'], 200);
    }
}
