<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Board;
use App\User;
use App\Lists;
use App\Card;

class CardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($board_id, $list_id)
    {
        $board = Board::find($board_id);

        $list = $board->lists()->find($list_id);

        return response()->json(['status'=>'succcess','cards'=>$list->cards]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $boardId,$listId)
    {
        $this->validate($request,['name'=>'required']);
        $board=Board::find($boardId);
        if (Auth::user()->id !== $board->user_id) {
            return response()->json(['status' => 'error', 'message' => 'unauthorized'], 401);
        }
        $board->lists()->find($listId)->cards()->create([
            'name'    => $request->name,
        ]);
        return response()->json(['message' => 'success'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($board_id, $list_id, $id)
    {
        $board = Board::find($board_id);

        $list = $board->lists()->find($list_id);

        return response()->json(['status'=>'succcess','card'=>$list->cards()->find($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $boardId,$listId,$cardId)
    {
        $this->validate($request,['name'=>'required']);

        $board = Board::find($boardId);
        if (Auth::user()->id !== $board->user_id) {
            return response()->json(['status' => 'error', 'message' => 'unauthorized'], 401);
        }

        $card=$board->lists()->find($listId)->cards()->find($cardId);
        $card->update($request->all());
        return response()->json(['message' => 'success', 'card' => $card], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($boardId,$listId,$cardId)
    {
        $board=Board::find($boardId);
        if(Auth::user()->id !== $board->user_id) {
            return response()->json(['status'=>'error','message'=>'unauthorized'],401);
        }
        $card=$board->lists()->find($listId)->cards()->find($cardId);
        if ($card->delete()) {
            return response()->json(['status' => 'success', 'message' => 'Card Deleted Successfully']);
        }
        return response()->json(['status' => 'error', 'message' => 'Something went wrong']);
    }
}
