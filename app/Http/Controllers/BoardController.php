<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Board;
use App\User;

class BoardController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth', ['only'=>['index','update','destroy']]);
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return \Auth::user()->boards;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $board = Board::create([
            'name'=> $request->name,
            'user_id'=> \Auth::id()
        ]);

        return Response()->json(['status'=> 'success', 'data'=> $board], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $board = Board::find($id);

        if( !$board) {
            return response()->json(['status'=> 'error', 'message'=> 'Data not found'], 401);
        }

        if ( \Auth::user()->id !== $board->user_id) {
            return Response()->json(['status'=> 'error', 'message'=> 'unauthorized'], 401);
        }
        return $board;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $board = Board::find($id);

        if ( \Auth::user()->id !== $board->user_id) {
            return Response()->json(['status'=> 'error', 'message'=> 'unauthorized'], 401);
        }

        $result = $board->update($request->all());

        //         print "<pre>";
        // print_r($request->all());exit;
        return Response()->json(['status'=> 'success', 'message'=> 'Updated', 'board'=>$board], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $board = Board::find($id);

        if ( \Auth::user()->id !== $board->user_id) {
            return Response()->json(['status'=> 'error', 'message'=> 'unauthorized'], 401);
        }

        if(Board::destroy($id)) {
            return Response()->json(['status'=> 'success', 'message'=> 'Deleted'], 200);
        }

        return Response()->json(['status'=> 'error', 'message'=> 'Something wentt wrong'], 200);
    }
}
