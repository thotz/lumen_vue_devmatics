<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    protected $fillable = ['list_id','name','description'];

    public function lists()
    {
        return $this->belongsTo(Lists::class);
    }
}
